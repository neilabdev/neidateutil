//
// Created by ghost on 4/5/13.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

extern NSString *const NEIDateFormatTimeAttributeName; //HH:mm
extern NSString *const NEIDateStyleTimeAttributeName;

extern NSString *const NEIDateFormatDateAttributeName;
extern NSString *const NEIDateStyleDateAttributeName;

extern NSString *const NEIDateFormatSecondsAttributeName;
extern NSString *const NEIDateStyleSecondsAttributeName;

extern NSString *const NEIDateStyleDaysAttributeName;
extern NSString *const NEIDateStyleDaysPrefixAttributeName;

extern NSString *const NEIDateStyleHoursAttributeName;
extern NSString *const NEIDateStyleHoursPrefixAttributeName;

extern NSString *const NEIDateStyleMinutesAttributeName;
extern NSString *const NEIDateStyleMinutesPrefixAttributeName;

@interface NSDate (NEIDateUtil)
- (nonnull NSDate *)toLocalTime NS_SWIFT_NAME(localTime());

- (nonnull NSDate *)toGlobalTime  NS_SWIFT_NAME(globalTime());

- (nonnull NSString *)stringWithShortDateOptions: (NSDictionary*) options  NS_SWIFT_NAME(shortDate(options:));
- (nonnull NSString *)stringWithShortDate  NS_SWIFT_NAME(shortDate());

- (nonnull NSString *)stringInDateFormat:(NSString *_Nonnull)format NS_SWIFT_NAME(date(in:));

- (nonnull NSString *)stringWithDateFormat:(NSString *_Nonnull)format  NS_SWIFT_NAME(date(formatted:));

+ (nonnull NSDate *)fromDate:(NSDate *)swiftDate  NS_SWIFT_NAME(from(date:));
@end