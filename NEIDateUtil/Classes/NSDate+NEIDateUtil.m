
#import "NSDate+NEIDateUtil.h"
#define THIS_OR_THAT(this, that) (this ? this : that)


NSString *const NEIDateFormatTimeAttributeName = @"NEIDateFormatTimeAttributeName";
NSString *const NEIDateStyleTimeAttributeName = @"NEIDateStyleTimeAttributeName";

NSString *const NEIDateFormatDateAttributeName = @"NEIDateFormatDateAttributeName";
NSString *const NEIDateStyleDateAttributeName = @"NEIDateStyleDateAttributeName";

NSString *const NEIDateFormatSecondsAttributeName = @"NEIDateFormatSecondsAttributeName";
NSString *const NEIDateStyleSecondsAttributeName = @"NEIDateStyleSecondsAttributeName";

NSString *const NEIDateStyleDaysAttributeName = @"NEIDateStyleDaysAttributeName";
NSString *const NEIDateStyleDaysPrefixAttributeName = @"NEIDateStyleDaysPrefixAttributeName";

NSString *const NEIDateStyleHoursAttributeName = @"NEIDateStyleHoursAttributeName";
NSString *const NEIDateStyleHoursPrefixAttributeName = @"NEIDateStyleHoursPrefixAttributeName";
NSString *const NEIDateStyleMinutesAttributeName = @"NEIDateStyleMinutesAttributeName";
NSString *const NEIDateStyleMinutesPrefixAttributeName = @"NEIDateStyleMinutesPrefixAttributeName";

@implementation NSDate (NEIDateUtil)

- (NSDate *_Nonnull)toLocalTime {
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate:self];
    return [NSDate dateWithTimeInterval:seconds sinceDate:self];
}

- (NSDate *_Nonnull)toGlobalTime {
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate:self];
    return [NSDate dateWithTimeInterval:seconds sinceDate:self];
}

- (NSString *_Nonnull)stringWithShortDateOptions:(NSDictionary *)params {
    NSDictionary *defaults = @{
            NEIDateFormatDateAttributeName: @"MM/dd",
            NEIDateStyleDateAttributeName: @"%@",

            NEIDateStyleDaysAttributeName: @"%@d",
            NEIDateStyleDaysPrefixAttributeName: @"-",

            NEIDateFormatTimeAttributeName: @"HH:mm",
            NEIDateStyleTimeAttributeName: @"%@",

            NEIDateStyleHoursAttributeName:@"%@h",
            NEIDateStyleHoursPrefixAttributeName: @"-",

            NEIDateStyleMinutesAttributeName: @"%@m",
            NEIDateStyleMinutesPrefixAttributeName: @"-",

            NEIDateFormatSecondsAttributeName: @"ss",
            NEIDateStyleSecondsAttributeName: @"%@s"
    };

    NSMutableDictionary *opts = [NSMutableDictionary dictionaryWithDictionary:defaults];

    if (params)
        [opts addEntriesFromDictionary:params];

    NSString *short_time = nil;
    NSDate *localTime = [self toLocalTime];
    NSDate *now = [[NSDate date] toLocalTime];
    NSTimeInterval intervalSinceNow = [localTime timeIntervalSinceDate:now]; // [localTime timeIntervalSinceNow];//  [self timeIntervalSinceNow ];
    NSTimeInterval interval = abs(intervalSinceNow);
    BOOL inFuture = intervalSinceNow > 0;

    if (interval > (60 * 60 * 24 * 7)) { // a week old+
        NSString *outputFormat = THIS_OR_THAT(opts[NEIDateFormatDateAttributeName], @"MM/dd");
        NSString *outputStyle = THIS_OR_THAT(opts[NEIDateStyleDateAttributeName], @"%@");
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:outputFormat];
        short_time = [NSString stringWithFormat:outputStyle, [outputFormatter stringFromDate:self]];
    } else if (interval > (60 * 60 * 24)) { // a day+ old
        NSString *daysStyle = THIS_OR_THAT(opts[NEIDateStyleDaysAttributeName], @"%@d"); // was @"%@%dm"
        NSString *daysPrefix = THIS_OR_THAT(opts[NEIDateStyleDaysPrefixAttributeName], @"-");
        NSString *timeDisplay = [NSString stringWithFormat:daysStyle, @((long) interval / (60 * 60 * 24))]; // 1d
        short_time = [NSString stringWithFormat:@"%@%@", (inFuture ? @"" : daysPrefix), timeDisplay];
    } else if (interval > (60 * 60 * 7)) { // > 7 hours
        NSString *outputFormat = THIS_OR_THAT(opts[NEIDateFormatTimeAttributeName],@"HH:mm");
        NSString *outputStyle = THIS_OR_THAT(opts[NEIDateStyleTimeAttributeName],@"%@");
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:outputFormat];
        short_time = [NSString stringWithFormat:outputStyle, [outputFormatter stringFromDate:self]];
    } else if (interval > (60 * 60 * 1)) { // > an hour
        NSString *hourStyle = THIS_OR_THAT(opts[NEIDateStyleHoursAttributeName], @"%@h");
        NSString *hourPrefix = THIS_OR_THAT(opts[NEIDateStyleHoursPrefixAttributeName], @"-");
        NSString *a = [NSString stringWithFormat:hourStyle, @((long) interval / (60 * 60 * 1)) ];
        short_time = [NSString stringWithFormat:@"%@%@", (inFuture ? @"" : hourPrefix), a];
    } else if (interval > (60 * 1)) {  // greater than a minute
        NSString *minuteStyle = THIS_OR_THAT(opts[NEIDateStyleMinutesAttributeName], @"%@m"); // was @"%@%dm"
        NSString *minutePrefix = THIS_OR_THAT(opts[NEIDateStyleMinutesPrefixAttributeName], @"-");
        NSString * a = [NSString stringWithFormat:minuteStyle, @( (long) interval / (60 * 1)) ];
        short_time = [NSString stringWithFormat:@"%@%@", (inFuture ? @"" :minutePrefix), a ];
    } else {
        NSString *outputFormat = THIS_OR_THAT(opts[NEIDateFormatSecondsAttributeName],@"ss");
        NSString *outputStyle = THIS_OR_THAT(opts[NEIDateStyleSecondsAttributeName],@"%@s");
        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        [outputFormatter setDateFormat:outputFormat];
        short_time = [NSString stringWithFormat:outputStyle, [outputFormatter stringFromDate:self]];
    }
    return short_time;
}

- (NSString *_Nonnull)stringWithShortDate {
    return [self stringWithShortDateOptions:nil];
}

- (NSString *_Nonnull)stringInDateFormat:(NSString *)format { //stringInDateFormat
    NSString *time_string = nil;
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"MMM dd, yyyy HH:mm"];
    time_string = [outputFormatter stringFromDate:self];
    return [NSString stringWithFormat:format, time_string];
}

- (NSString *_Nonnull)stringWithDateFormat:(NSString *)format { //
    NSString *time_string = nil;
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:format];
    time_string = [outputFormatter stringFromDate:self];
    return time_string; //[NSString stringWithFormat: format , time_string ];
}

+ (NSDate *_Nonnull)fromDate:(NSDate *)date {
    return [NSDate dateWithTimeIntervalSince1970:date.timeIntervalSince1970];
}
@end