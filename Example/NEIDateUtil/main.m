//
//  main.m
//  NEIDateUtil
//
//  Created by James Whitfield on 05/16/2017.
//  Copyright (c) 2017 James Whitfield. All rights reserved.
//

@import UIKit;
#import "NEIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NEIAppDelegate class]));
    }
}
