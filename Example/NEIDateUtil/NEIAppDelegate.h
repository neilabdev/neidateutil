//
//  NEIAppDelegate.h
//  NEIDateUtil
//
//  Created by James Whitfield on 05/16/2017.
//  Copyright (c) 2017 James Whitfield. All rights reserved.
//

@import UIKit;

@interface NEIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
