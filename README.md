# NEIDateUtil

[![CI Status](http://img.shields.io/travis/James Whitfield/NEIDateUtil.svg?style=flat)](https://travis-ci.org/James Whitfield/NEIDateUtil)
[![Version](https://img.shields.io/cocoapods/v/NEIDateUtil.svg?style=flat)](http://cocoapods.org/pods/NEIDateUtil)
[![License](https://img.shields.io/cocoapods/l/NEIDateUtil.svg?style=flat)](http://cocoapods.org/pods/NEIDateUtil)
[![Platform](https://img.shields.io/cocoapods/p/NEIDateUtil.svg?style=flat)](http://cocoapods.org/pods/NEIDateUtil)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NEIDateUtil is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "NEIDateUtil"
```

## Author

James Whitfield, jwhitfield@neilab.com

## License

NEIDateUtil is available under the MIT license. See the LICENSE file for more info.
